/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('gui')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
