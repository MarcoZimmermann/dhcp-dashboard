(function() {
    'use strict';

    angular
        .module('gui')
        .factory('leasesService', leasesService);

    /** @ngInject */
    function leasesService($log, $http) {
        var apiHost = 'http://192.168.2.99/api';
        //var apiHost = 'http://localhost:3300';
        var macApi = "http://api.macvendors.com/"
      
        var service = {
            getLeases: getLeases,
            getMacManufacturer : getMacManufacturer
        };

        return service;

        
      function getMacManufacturer(macAddress) {
          return $http.get(macApi+macAddress)
           .then(function (response) {
                return response.data;
            });
      }
      
      function getLeases() {
           return $http.get(apiHost)
           .then(getLeasesComplete)
            .catch(getLeasesFailed);

        function getLeasesComplete(response) {
            response.data.forEach(function(element) {
                // getMacManufacturer(element.data["hardware ethernet"]).then(function (macManufacturer) {
                //     element.data.macManufacturer = macManufacturer;
                // });                
            }, this);
            
            return response.data;
        }

        function getLeasesFailed(error) {
            $log.error('XHR Failed for getLeases.\n' + angular.toJson(error.data, true));
        }
      }
    }

})();