(function() {
  'use strict';

  angular
    .module('gui')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(leasesService, moment) {
    var vm = this;
    
    vm.leases = [];
    leasesService.getLeases().then(function (data) {
      vm.leases = data.map(function(item) {
        item.data.starts = moment(new Date(item.data.starts)).format('lll');
        item.data.ends = moment(new Date(item.data.ends)).format('lll');
        return item;
      });
    });
   
  }
})();
