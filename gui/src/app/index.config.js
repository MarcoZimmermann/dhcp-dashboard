(function() {
  'use strict';

  angular
    .module('gui')
    .config(config);

  /** @ngInject */
  function config($logProvider, moment) {
    // Enable log
    $logProvider.debugEnabled(true);

   moment.locale('de');
  }

})();
