var express = require('express');
var router = express.Router();

var fs = require('fs'); 
var dhcpdleases = require('dhcpd-leases'); 

//var leaseFile = '/var/lib/dhcp/dhcpd.leases';
var leaseFile = '/home/wesley/Dokumente/dhcpd.leases';

/* GET home page. */
router.get('/', function(req, res, next) {
  
  fs.readFile( leaseFile, function(error, data) {
        if (error) {
            res.setHeader('Content-Type', 'application/json');
            res.json(JSON.stringify({
                'error':'Server error',
                'message':error
            }));
        }
        else {
            var leases = fs.readFileSync(leaseFile, 'utf-8');
            var data = dhcpdleases(leases);
            var leasesArray = [];
            var index=0;
            for (var prop in data) {
                if (data.hasOwnProperty(prop)) {
                    var element = data[prop];
                    leasesArray[index] = {
                        ip: prop,
                        data: element  
                    };
                    index++;
                }
            }
            
            res.setHeader('Content-Type', 'application/json');
            res.status(200);
            res.end(JSON.stringify(leasesArray));
        }
    });
});

module.exports = router;
