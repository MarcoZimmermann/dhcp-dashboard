(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(leasesService) {
    var vm = this;
    
    vm.leases = [];
    leasesService.getLeases().then(function (data) {
      vm.leases = data;
    });
   
  }
})();
